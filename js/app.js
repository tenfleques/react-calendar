
var datesList = [{date:"09.10.2017", color:"#0f0"},
{date:"12.10.2017", color:"#00f"}];
function isDefined(variable){
	return !(typeof variable === 'undefined' || variable===null);
}
function getFirstMonday(){
  var date = new Date()
  var month = date.getMonth(), year = date.getFullYear();
  if(isDefined(arguments[0])){
    month = arguments[0];
  }
  if(isDefined(arguments[1]))
    year = arguments[1];
  var d = new Date(year, month, 1, 0, 0, 0, 0)
  var day = 0
  // check if first of the month is a Sunday, if so set date to the second
  if (d.getDay() == 0) {
    day = 2
    d = d.setDate(day)
    d = new Date(d)
  }
  // check if first of the month is a Monday, if so return the date, otherwise get to the Monday following the first of the month
  else if (d.getDay() != 1) {
    day = 9-(d.getDay())
    d = d.setDate(day)
    d = new Date(d)
  }
  return d
}
function getColor(){
  var index = ('0'+arguments[2]).slice(-2)+"."+('0'+(arguments[1]+1)).slice(-2)+"."+arguments[0]
  var col = datesList.find(function(val){
    return val.date == index;
  })
  if(col)
    return col.color;
  return "#FFF";
}

var Calendar = React.createClass({
    displayName: 'календаря',
    calc: function (year, month) {
        if (this.state.selectedElement) {
            if (this.state.selectedMonth != month || this.state.selectedYear != year) {
                this.state.selectedElement.classList.remove('selected');
            } else {
                this.state.selectedElement.classList.add('selected');
            }
        }
        return {
            firstOfMonth: new Date(year, month, 1),
            daysInMonth: new Date(year, month + 1, 0).getDate(),
        };
    },
    componentWillMount: function () {
        this.setState(this.calc.call(null, this.state.year, this.state.month));
    },
    componentDidMount: function () {},
    componentDidUpdate: function (prevProps, prevState) {
        if (this.props.onSelect && prevState.selectedDt != this.state.selectedDt) {
            this.props.onSelect.call(this.getDOMNode(), this.state);
        }
    },
    getInitialState: function () {
        var date = new Date();
        var initialDay = getFirstMonday(date.getMonth(),date.getFullYear());
        return  {
            year: date.getFullYear(),
            month: date.getMonth(),
            selectedYear: date.getFullYear(),
            selectedMonth: date.getMonth(),
            selectedDate: initialDay.getDate(),
            selectedDt: initialDay,
            startDay: 1,
            minDate: this.props.minDate ? this.props.minDate : null,
            dayNames: ["вс","пн", "вт", "ср", "чт", "пт", "сб"],
            monthNames: ["янв", "Фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"],
            monthNamesFull: ["Январь", "Февраль", "Марта", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            firstOfMonth: null,
            daysInMonth: null
        };

    },
    getPrev: function () {
        var state = {};
        if (this.state.month > 0) {
            state.month = this.state.month - 1;
            state.year = this.state.year;
        } else {
            state.month = 11;
            state.year = this.state.year - 1;
        }
        Object.assign(state, this.calc.call(null, state.year, state.month));
        this.setState(state);
        console.log("current month", this.state.monthNamesFull[state.month]);
    },
    getNext: function () {
        var state = {};
        if (this.state.month < 11) {
            state.month = this.state.month + 1;
            state.year = this.state.year;
        } else {
            state.month = 0;
            state.year = this.state.year + 1;
        }
        Object.assign(state, this.calc.call(null, state.year, state.month));
        this.setState(state);
        console.log("current month", this.state.monthNamesFull[state.month])
    },
    selectDate: function (year, month, date, element) {
        this.state.selectedElement = document.querySelector("div.cell.selected")
        if(this.state.selectedElement)
          this.state.selectedElement.classList.remove('selected');
        element.target.classList.add('selected');
        this.setState({
            selectedYear: year,
            selectedMonth: month,
            selectedDate: date,
            selectedDt: new Date(year, month, date),
            selectedElement: element.target
        });
    },
    render: function () {
        return React.createElement(
            'div',
            { className: 'calendar' },
            React.createElement(
                'div',
                { className: 'inner' },
                React.createElement(Header, { monthNames: this.state.monthNamesFull, month: this.state.month, year: this.state.year, onPrev: this.getPrev, onNext: this.getNext }),
                React.createElement(MonthDates, { month: this.state.month, year: this.state.year, daysInMonth: this.state.daysInMonth, firstOfMonth: this.state.firstOfMonth, startDay: this.state.startDay, onSelect: this.selectDate})
            )
        );
    }
});

var Header = React.createClass({
    displayName: 'Глава',

    render: function () {
        return React.createElement(
            'div',
            { className: 'row head' },
            React.createElement('div', { className: 'cell prev', onClick: this.props.onPrev.bind(this), role: 'button', tabIndex: '0' }),
            React.createElement(
                'div',
                { className: 'cell title' },
                this.props.monthNames[this.props.month],
                ' ',
                this.props.year
            ),
            React.createElement('div', { className: 'cell next', onClick: this.props.onNext.bind(this), role: 'button', tabIndex: '0' })
        );
    }
});


var MonthDates = React.createClass({
    displayName: 'MonthDates',
    statics: {
        year: getFirstMonday().getFullYear(),
        month: getFirstMonday().getMonth(),
        date: getFirstMonday().getDate()
    },
    render: function () {
        var haystack,
            day,
            id,
            d,
            onClick,
            isDate,
            className,
            weekStack = Array.apply(null, { length: 7 }).map(Number.call, Number),
            that = this,
            startDay = this.props.firstOfMonth.getUTCDay(),
            first = this.props.firstOfMonth.getDay(),
            janOne = new Date(that.props.year, 0, 1),
            rows = 5;
        if (startDay == 5 && this.props.daysInMonth == 31 || startDay == 6 && this.props.daysInMonth > 29) {
            rows = 6;
        }

        className = rows === 6 ? 'dates' : 'dates fix';
        haystack = Array.apply(null, { length: rows }).map(Number.call, Number);
        day = this.props.startDay + 1 - first;
        while (day > 1) {
            day -= 7;
        }
        day -= 1;
        return React.createElement(
            'div',
            { className: className },
            haystack.map(function (item, i) {
                d = day + i * 7;
                return React.createElement(
                    'div',
                    { className: 'row' },
                    weekStack.map(function (item, i) {
                        d += 1;
                        isDate = d > 0 && d <= that.props.daysInMonth;

                        if (isDate) {
                            className = 'cell date';
                            var current = new Date(that.props.year, that.props.month, d);
                            var stdate = getFirstMonday(that.props.month,that.props.year);
                            
                            if(current.getTime() === stdate.getTime()){
                              className = 'cell date selected';
                            }
                            return React.createElement(
                                'div',
                                {style: {color:getColor(that.props.year,that.props.month,d)},className: className, role: 'button', tabIndex: '0', onClick: that.props.onSelect.bind(that, that.props.year, that.props.month, d) },
                                d
                            );
                        }
                        return React.createElement('div', { className: 'cell' });
                    })
                );
            })
        );
    }
});

ReactDOM.render(React.createElement(Calendar, {
    onSelect: function (state) {
      //"selected day", day
    console.log("selected day", state.selectedDate);
    }
}), document.getElementById("calendar"));
